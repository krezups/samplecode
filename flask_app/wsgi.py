#!/usr/bin/python
from doctest import debug
import sys

sys.path.insert(0,"/var/www/FLASK_APP/")

from api import app as application

if __name__=='__main__':
   application.run(debug=True)