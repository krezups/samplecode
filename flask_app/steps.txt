#! /bin/bash

aws configure set output json
aws configure set region us-east-1
aws s3 cp s3://attrition-dev/attrition-api-code-deployment/attrition-api-main.zip .
unzip attrition-api-main.zip
rm attrition-api-main.zip
echo 1 > version_attrition_app.txt
cd attrition-api-main
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
gunicorn --pid PID_FILE --bind 0.0.0.0:5000 wsgi:app

if python3 tests/testcases.py ; 
then
    echo "Command succeeded"
	kill $(cat PID_FILE)
	setenforce 0
	

	//sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/nginx/config
	//sed -i 's/SELINUX=permissive/SELINUX=disabled/g' /etc/nginx/config
	
	

	echo "">/etc/systemd/system/attrition-api-app.service
	while read line; do    
		echo $line >> /etc/systemd/system/attrition-api-app.service   
	done < ../helper_text/attrition-api-app.service.txt
	
	
	systemctl start attrition-api-app
	systemctl enable app
	
	
	filename_sock=app.sock      # check for file name it may be attrition-api-app.sock
	if test -f "$filename";
	then
		echo "$filename has found."
		
		
		searchString="include /etc/nginx/conf.d/*.conf"
		if ! grep -Fxq "$searchString" /etc/nginx/nginx.conf;
		then
		  sed -i 's/http{/http{ \ninclude /etc/nginx/conf.d/*.conf;\n/g' /etc/nginx/config
		fi
	
	
	
		rm /etc/nginx/conf.d/default.conf
		
		
		echo "">/etc/nginx/conf.d/attrition-api-app.conf
		while read line; do    
			echo $line >> /etc/nginx/conf.d/attrition-api-app.conf   
		done < ../helper_text/attrition-api-app.conf.txt
		
		
		
		
		
		
		echo "">/etc/nginx/proxy_params
		while read line; do    
			echo $line >> /etc/nginx/proxy_params
		done < ../helper_text/proxy_params.txt
		
		
		
		
		
		
		
		
		echo "">/etc/ufw/applications.d/nginx.ini
		while read line; do    
			echo $line >> /etc/ufw/applications.d/nginx.ini
		done < ../helper_text/nginx.ini.txt
		
		
		
		
		
		
		
		ufw delete allow 5000
		ufw allow 'Nginx  Full'
		
		
		sed -i 's/user nginx/user root/g' /etc/nginx/nginx.conf
		
		
		systemctl start nginx
			
	else
		echo "$filename has not been created hence can't deploy app"
		
		kill $(cat PID_FILE)
		cd ..
		rm -r attrition-api-main
		cd backup-versions
		mv attrition-api-main-v$variable ..
		cd ..
		mv attrition-api-main-v$variable attrition-api-main
		echo $(($variable)) > version_attrition_app.txt
		
	fi
	
	
	
	
	
	
else
    echo "Command failed"
	kill $(cat PID_FILE)
	cd ..
	rm -r attrition-api-main
	cd backup-versions
	mv attrition-api-main-v$variable ..
	cd ..
	mv attrition-api-main-v$variable attrition-api-main
	echo $(($variable)) > version_attrition_app.txt
fi
